![overview](docs/img3.png)

# IQCELL (v 1.0.0)

## Overview
IQCELL is an analysis tool developed in Zandstra lab to infer, analyzes, and simulates gene regulatory networks (GRNs) from the single-cell scRNA-seq data of the cells undergoing development. 

[IQCELL: A platform for predicting the effect of gene perturbations on developmental trajectories using single-cell RNA-seq data](https://journals.plos.org/ploscompbiol/article?id=10.1371/journal.pcbi.1009907)

## Installation
[Installation of IQCELL](https://gitlab.com/stemcellbioengineering/iqcell/-/blob/master/installation%20/instalation.md) is a two step process:
1. Step 1: installing all dependencies.
2. Step 2: installing IQCELL. 

Please read the installation.md in the installation directory

## A quick guide on how to use IQCELL

There are three example notebooks provided in this quick guide (Notebooks 1, 2, and 3):

Please follow the visual guide bellow to analyze your scRNA-seq data according to this guide.
![workflow](docs/img4.png)

- [Notebook 1](https://gitlab.com/stemcellbioengineering/iqcell/-/blob/master/Notebooks/Notebook1): Provides an example of preprocessing scRNA-seq data.
- [Notebook 2](https://gitlab.com/stemcellbioengineering/iqcell/-/blob/master/Notebooks/Notebook2): Provides an example of selecting informative genes.
- [Notebook 3](https://gitlab.com/stemcellbioengineering/iqcell/-/blob/master/Notebooks/Notebook3): Provides an example of running IQCELL gene regulatory inference and analysis.


