## Installation: (tested on mac and Linux)
Please follow the steps below to install iqcell. We have tested iqcell in Linux and macOs. If you are running a Windows operating system, we recommend using [Virtual box](https://www.virtualbox.org/) and install a Linux virtual machine. 


1-Please install [Conda](https://docs.conda.io/en/latest/miniconda.html) 

2-Create a new environment called iqcell (with a python version >= 3.7):

`conda create --name iqcell`

`conda activate iqcell`

`conda install python==3.7.3`

3-Please make sure these libraries are installed:

numpy: `pip install numpy==1.21.6` 

matplotlib: `pip install matplotlib==2.2.3`

pandas: `pip install pandas==0.24.2`

sklearn: `pip install sklearn`

scipy: `pip install scipy==1.7.3`

seaborn: `pip install seaborn==0.11.2`

scprep `conda install -c bioconda scprep`

networkx: `conda install networkx=2.3`

magic: `pip install --user magic-impute==3.0.0`

pygraphviz: `conda install pygraphviz`

z3: `pip install z3-solver==4.8.17.0`

booleannet3: `pip uninstall ply`, `pip uninstall pyhcl`, next `pip install ply`, `pip install pyhcl`, finally go to [this page](https://github.com/joelostblom/booleannet3), download (clone) the contents, go to the directory and install via `python setup.py install`.

4-Now you should be able to download and install iqcell: download (clone) the [iqcell_package](https://gitlab.com/stemcellbioengineering/iqcell/-/blob/master/iqcell_package.zip). unzip and navigate to the directory and install via `python setup.py install`.

5-Install jupyter to be able to work with notebooks: conda `install jupyter`

6-Test installations: open jupyert notebook via `jupyter notebook`, create a new python3 notebook and test if you can `import iqcell`

Now you should be able to use `Notebook3`










